﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebApplication4.Models;

namespace WebApplication4.Context
{
    public class UDal
    {
        readonly string connectionstring = "Data Source=LAPTOP-ED4305VJ;Initial Catalog=test;Integrated Security=SSPI;";
        public IEnumerable<User> GetAllUser() 
        {
            var userList = new List<User>();
            using (SqlConnection con = new SqlConnection(connectionstring)) 
            {
                SqlCommand cmd = new SqlCommand("SP_GetAllUser", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var user = new User();
                    user.user_id = Convert.ToInt32(dr["ID"].ToString());
                    user.name = dr["name"].ToString();
                    user.email = dr["email"].ToString();
                    user.phone = dr["phone"].ToString();
                    user.address = dr["address"].ToString();

                    userList.Add(user);
                }
                con.Close();
            }
            return userList;
        }

        public void CreateUser(User user) 
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand("SP_CreateUser", con)
                {
                    CommandType = CommandType.StoredProcedure
                };
                cmd.Parameters.AddWithValue("@name", user.name);
                cmd.Parameters.AddWithValue("@email", user.email);
                cmd.Parameters.AddWithValue("@phone", user.phone);
                cmd.Parameters.AddWithValue("@address", user.address);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void UpdateUser(User user)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand("SP_UpdateUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@ID", user.user_id);
                cmd.Parameters.AddWithValue("@name", user.name);
                cmd.Parameters.AddWithValue("@email", user.email);
                cmd.Parameters.AddWithValue("@phone", user.phone);
                cmd.Parameters.AddWithValue("@address", user.address);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public void DeleteUser(int? user_id)
        {
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand("SP_DeleteUser", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.AddWithValue("@ID", user_id);


                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        public User GetUserById(int? user_id)
        {
            var user = new User();
            using (SqlConnection con = new SqlConnection(connectionstring))
            {
                SqlCommand cmd = new SqlCommand("SP_GetUserById", con)
                {
                    CommandType = CommandType.StoredProcedure
                };

                cmd.Parameters.AddWithValue("@ID", user_id);


                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    user.user_id = Convert.ToInt32(dr["ID"].ToString());
                    user.name = dr["name"].ToString();
                    user.email = dr["email"].ToString();
                    user.phone = dr["phone"].ToString();
                    user.address = dr["address"].ToString();
                }
                con.Close();
            }
            return user;
        }

    }
}
